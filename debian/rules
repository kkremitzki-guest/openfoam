#!/usr/bin/make -f

DEB_HOST_ARCH ?= $(shell dpkg-architecture -qDEB_HOST_ARCH)
VENDOR := $(shell dpkg-vendor --derives-from Ubuntu && echo Ubuntu || echo Debian)

# less debug info to avoid running out of address space during build
ifneq (,$(filter $(DEB_HOST_ARCH), mips mipsel))
    export DEB_CXXFLAGS_MAINT_APPEND = -g1
endif

export DEB_BUILD_MAINT_OPTIONS = hardening=+all

%:
	dh $@

export FOAM_INST_DIR=/usr
export FOAM_APP=$(CURDIR)/applications
export FOAM_SOLVERS=$(CURDIR)/applications/solvers
export FOAM_APPBIN=$(CURDIR)/platforms/linux64GccDPInt32Opt/bin
export FOAM_EXT_LIBBIN=$(CURDIR)/platforms/linux64Gcc51/lib
export FOAM_JOB_DIR=$(CURDIR)/jobControl
export FOAM_LIBBIN=$(CURDIR)/platforms/linux64GccDPInt32Opt/lib
export FOAM_MPI=openmpi-system
export FOAM_RUN=$(CURDIR)/run
export FOAM_SITE_APPBIN=$(CURDIR)/platforms/linux64GccDPInt32Opt/bin
export FOAM_SITE_LIBBIN=$(CURDIR)/platforms/linux64GccDPInt32Opt/lib
export FOAM_SRC=$(CURDIR)/src
export FOAM_ETC=$(CURDIR)/etc
export FOAM_USER_APPBIN=$(CURDIR)/platforms/linux64GccDPInt32Opt/bin
export FOAM_USER_LIBBIN=$(CURDIR)/platforms/linux64GccDPInt32Opt/lib
export MANPATH=$(CURDIR)/platforms/linux64Gcc/openmpi-3.1.1/share/man
export MPI_ARCH_PATH=/usr/include/openmpi
export MPI_BUFFER_SIZE=20000000
export WM_ARCH=linux64
export WM_ARCH_OPTION=$(shell dpkg-architecture -qDEB_HOST_ARCH_BITS)
export WM_LABEL_SIZE=32
export WM_LABEL_OPTION=Int32
export WM_CC=gcc
export WM_CFLAGS=-m64 -fPIC -I/usr/include/openmpi
export WM_COMPILER=Gcc
export WM_COMPILER_LIB_ARCH=64
export WM_COMPILE_OPTION=Opt
export WM_CXX=g++
export WM_CXXFLAGS=-fPIC -I/usr/include/openmpi -std=c++0x
export WM_DIR=$(CURDIR)/wmake
export WM_LDFLAGS=-I/usr/include/openmpi -m64
export WM_LINK_LANGUAGE=c++
export WM_OSTYPE=POSIX
export WM_OPTIONS=linux64GccDPInt32Opt
export WM_PRECISION_OPTION=DP
export WM_PROJECT=OpenFOAM
export WM_PROJECT_DIR=$(CURDIR)
export WM_PROJECT_INST_DIR=$(CURDIR)/debian/tmp/usr
export WM_PROJECT_USER_DIR=$(CURDIR)/debian/tmp
export WM_PROJECT_VERSION=6.20180805
export gperftools_install=$(CURDIR)/platforms/linux64Gcc
export WM_MPLIB=SYSTEMOPENMPI
unexport FOAMY_HEX_MESH

NUMJOBS := 1
ifneq (,$(filter parallel=%,$(DEB_BUILD_OPTIONS)))
    NUMJOBS := $(patsubst parallel=%,%,$(filter parallel=%,$(DEB_BUILD_OPTIONS)))
endif
# Limit parallel build to 3 processes on Ubuntu amd64, arm64 and ppc64el
ifeq ($(VENDOR),Ubuntu)
ifneq (,$(filter $(DEB_HOST_ARCH),amd64 arm64 ppc64el))
    NUMJOBS := $(shell if test $(NUMJOBS) -gt 3; then echo 3 ; else echo $(NUMJOBS); fi)
endif
endif

export WM_NCOMPPROCS=$(NUMJOBS)

export LD_LIBRARY_PATH:=$(CURDIR)/platforms/linux64GccDPInt32Opt/lib/openmpi-system:/usr/lib/openmpi/lib:$(CURDIR)/platforms/linux64GccDPInt32Opt/lib:$(CURDIR)/site/3.0.x/platforms/linux64GccDPInt32Opt/lib:$(CURDIR)/platforms/linux64GccDPInt32Opt/lib:$(CURDIR)/platforms/linux64GccDPInt32Opt/lib/dummy:$(CURDIR)/src/:$(LD_LIBRARY_PATH);

override_dh_auto_build:
	cd $(CURDIR)/debian/manpage ; python3 gen_man.py
	PATH=$(PATH):$(CURDIR)/wmake/:$(CURDIR)/etc ./Allwmake -j$(NUMJOBS)

override_dh_missing-arch:
	dh_missing --fail-missing
